from scapy.layers.inet import TCP

from constants.State import State


def get_packet_mock(state, is_push=False):
    assert state is not None

    if state == State.SYN:
        return TCP(flags="S" + "P" if is_push else "")

    if state == State.SYN_ACK:
        return TCP(flags="SA" + "P" if is_push else "")

    if state == State.ACK:
        return TCP(flags="A" + "P" if is_push else "")

    if state == State.FIN:
        return TCP(flags="F" + "P" if is_push else "")
