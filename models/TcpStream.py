from collections import namedtuple


class TcpStream(object):
    def __init__(self, our_ip, our_port, their_ip, their_port):
        self.our_ip = our_ip
        self.our_port = our_port
        self.their_ip = their_ip
        self.their_port = their_port

        # self.sent_packet = None
        self.sent_state = None
        self.received_state = None

        self.prev_sent_state = None
        self.prev_received_state = None

        # self.received_packet = None



"""class TcpStreamSide:
    def __init__(self, ip, port, stage=NO_STAGE):
        self.ip = ip
        self.port = port

        self.stage = stage

    @property
    def active(self):
        return is_handshaked(self.stage)"""

"""class TcpStream:
    def __init__(self, src_side, dst_side):
        self.src_side = src_side
        self.dst_side = dst_side

    @property
    def active(self):
        return self.src_side.active and self.dst_side.active
"""
