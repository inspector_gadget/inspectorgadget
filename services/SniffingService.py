import socket
from scapy.all import *
from scapy.layers.inet import *

from constants.State import State, state_names
from models.TcpStream import TcpStream
from services.PacketsService import BadPacket, FirstPacketIsNotSynError
from services.StreamService import StreamEnded


def exactly_one(iterable):
    result = False
    for item in iterable:
        if item is True:
            if result is True:
                result = False
                break
            else:
                result = True
    return result


class SniffingService(object):
    def __init__(self, ips_service, packets_service, streams_service, stream_service):
        self.ips_service = ips_service
        self.packets_service = packets_service
        self.streams_service = streams_service
        self.stream_service = stream_service

    def sniff_filter(self, pac):
        return TCP in pac and (pac[IP].dst in self.ips_service.get_ips() or pac[IP].src in self.ips_service.get_ips())

    def sniff(self):
        sniff(prn=lambda pac: self.analyze_packet(pac), lfilter=self.sniff_filter)

    def get_relevant_connection_details(self, pac):
        connection_details = []

        if self.ips_service.are_we_packet_source(pac):
            our_ip, our_port = pac[IP].src, pac[TCP].sport
            their_ip, their_port = pac[IP].dst, pac[TCP].dport

            connection_details.append((our_ip, our_port, their_ip, their_port))

        if self.ips_service.are_we_packet_destination(pac):
            our_ip, our_port = pac[IP].dst, pac[TCP].dport
            their_ip, their_port = pac[IP].src, pac[TCP].sport

            connection_details.append((our_ip, our_port, their_ip, their_port))

        return connection_details

    def get_stream_state_updated_text(self, stream, pac):

        if (stream.sent_state, stream.received_state) == (State.SYN, None):
            return "Sent SYN. TCP conversation from our port %d to %s:%d just started!" \
                   % (stream.our_port, stream.their_ip, stream.their_port)

        if (stream.sent_state, stream.received_state) == (None, State.SYN):
            return "Received SYN. TCP conversation from %s:%d to our port %d just started!" \
                   % (stream.their_ip, stream.their_port, stream.our_port)

        if (stream.sent_state, stream.received_state) == (State.SYN, State.SYN_ACK):
            return "Received SYN-ACK as part of the TCP conversation from our port %d to %s:%d" \
                   % (stream.our_port, stream.their_ip, stream.their_port)

        if (stream.sent_state, stream.received_state) == (State.SYN_ACK, State.SYN):
            return "Sent SYN-ACK as part of the TCP conversation from %s:%d to our port %d" \
                   % (stream.their_ip, stream.their_port, stream.our_port)

        if (stream.sent_state, stream.received_state) == (State.ACK, State.SYN_ACK):
            return "Sent ACK as part of the TCP conversation from our port %d to %s:%d. Connection Established!" \
                   % (stream.our_port, stream.their_ip, stream.their_port)

        if (stream.sent_state, stream.received_state) == (State.SYN_ACK, State.ACK):
            return "Received ACK as part of the TCP conversation from %s:%d to our port %d. Connection Established!" \
                   % (stream.their_ip, stream.their_port, stream.our_port)

        if (stream.sent_state, stream.received_state) in [(State.ACK, State.ACK), (State.SYN_ACK, State.ACK),
                                                          (State.ACK, State.SYN_ACK)] \
                and (stream.prev_sent_state, stream.prev_received_state) in [(State.ACK, State.ACK),
                                                                             (State.SYN_ACK, State.ACK),
                                                                             (State.ACK, State.SYN_ACK)]:

            if self.stream_service.are_we_packet_source(stream, pac):
                return "Sent ACK as part of the TCP conversation from our port %d to %s:%d" \
                       % (stream.our_port, stream.their_ip, stream.their_port)
            else:
                return "Received ACK as part of the TCP conversation from %s:%d to our port %d" \
                       % (stream.their_ip, stream.their_port, stream.our_port)

        if (stream.prev_sent_state, stream.prev_received_state) == (State.FIN, State.FIN):
            if (stream.sent_state, stream.received_state) == (State.ACK, State.FIN):
                return "Sent ACK as part of the TCP conversation disconnection from our port %d to %s:%d. Disconnected!" \
                       % (stream.our_port, stream.their_ip, stream.their_port)

            if (stream.sent_state, stream.received_state) == (State.FIN, State.ACK):
                return "Received ACK as part of the TCP conversation disconnection from %s:%d to our port %d. Disconnected!" \
                       % (stream.their_ip, stream.their_port, stream.our_port)

        if exactly_one(state == State.FIN for state in [stream.received_state, stream.sent_state]):
            if self.stream_service.are_we_packet_source(stream, pac):
                return "Sent FIN as part of the TCP conversation from our port %d to %s:%d. Starting disconnection!" \
                       % (stream.our_port, stream.their_ip, stream.their_port)
            else:
                return "Received FIN as part of the TCP conversation from %s:%d to our port %d. Starting disconnection!" \
                       % (stream.their_ip, stream.their_port, stream.our_port)

        if exactly_one(state == State.FIN for state in [stream.prev_received_state, stream.prev_sent_state]) and \
                exactly_one(state == State.ACK for state in [stream.received_state, stream.sent_state]):

            if self.stream_service.are_we_packet_source(stream, pac):
                return "Sent ACK as part of the TCP conversation disconnection from our port %d to %s:%d" \
                       % (stream.our_port, stream.their_ip, stream.their_port)
            else:
                return "Received ACK as part of the TCP conversation disconnection from %s:%d to our port %d" \
                       % (stream.their_ip, stream.their_port, stream.our_port)

        if (stream.sent_state, stream.received_state) == (State.FIN, State.FIN):
            if self.stream_service.are_we_packet_source(stream, pac):
                return "Sent FIN as part of the TCP conversation disconnection from our port %d to %s:%d" \
                       % (stream.our_port, stream.their_ip, stream.their_port)
            else:
                return "Received FIN as part of the TCP conversation disconnection from %s:%d to our port %d" \
                       % (stream.their_ip, stream.their_port, stream.our_port)

        return None

    def analyze_packet(self, pac):
        for our_ip, our_port, their_ip, their_port in self.get_relevant_connection_details(pac):

            stream = self.streams_service.find(our_ip, our_port, their_ip, their_port)

            if stream is None:
                stream = self.streams_service.create(TcpStream(our_ip, our_port, their_ip, their_port))

            try:
                is_stream_ended = self.stream_service.analyze_packet(stream, pac)

                if self.get_stream_state_updated_text(stream, pac) is not None:
                    print self.get_stream_state_updated_text(stream, pac)
                    print ""

                if is_stream_ended:
                    self.streams_service.remove(stream)

            except FirstPacketIsNotSynError:
                pass
            except BadPacket:
                if self.stream_service.are_we_packet_source(stream, pac):
                    print "Packet we sent from our port %d to %s:%d is not matching the stream state!" \
                          % (stream.our_port, stream.their_ip, stream.their_port)

                    print "Antepenultimate (Previous of previous) packet we sent: %s" % state_names[stream.prev_sent_state]
                    print "Previous packet we received: %s" % state_names[stream.prev_received_state]
                    print "Previous packet we sent: %s" % state_names[stream.sent_state]
                    print "Last packet we received: %s" % state_names[stream.received_state]
                    print "Packet we sent: %s" % state_names[self.packets_service.get_packet_state(pac)]

                else:
                    print "Packet we received from %s:%d to our port %d is not matching the stream state!" \
                          % (stream.their_ip, stream.their_port, stream.our_port)

                    print "Previous packet we sent: %s" % state_names[stream.prev_sent_state]
                    print "Antepenultimate (Previous of previous) packet we received: %s" % stream[state_names.prev_received_state]
                    print "Last packet we sent: %s" % state_names[stream.sent_state]
                    print "Previous packet we received: %s" % state_names[stream.received_state]
                    print "Packet we received: %s" % state_names[self.packets_service.get_packet_state(pac)]

                print ""
            except Exception as err:
                print err
                raise err


"""def on_packet_old(self, pac):
    src_to_dst_stream = self.streams_service.get_stream(pac[IP].dst, pac[TCP].dport, pac[IP].src, pac[TCP].sport)

    if src_to_dst_stream is None:
        try:
            pac_state = self.packets_service.get_dst_packet_state(pac)
            if pac_state == State.GOT_SYN:
                self.streams_service.create_stream(pac[IP].dst, pac[TCP].dport, pac[IP].src, pac[TCP].sport, pac_state)
        except FirstPacketIsNotSynError:
            print "First packet is not syn!!!"
    else:
        src_to_dst_stream.our_state = self.packets_service.get_dst_packet_state(pac, src_to_dst_stream.our_state)
        if src_to_dst_stream.is_ended:
            self.streams_service.delete_stream(src_to_dst_stream)

    dst_to_src_stream = self.streams_service.get_stream(pac[IP].src, pac[TCP].sport, pac[IP].dst, pac[TCP].dport)

    if dst_to_src_stream is None:
        try:
            pac_state = self.packets_service.get_src_packet_state(pac)
            if pac_state == State.SENT_SYN:
                self.streams_service.create_stream(pac[IP].src, pac[TCP].sport, pac[IP].dst, pac[TCP].dport, pac_state)
        except FirstPacketIsNotSynError:
            print "First packet is not syn!!!"
    else:
        dst_to_src_stream.our_state = self.packets_service.get_src_packet_state(pac, dst_to_src_stream.our_state)
        if dst_to_src_stream.is_ended:
            self.streams_service.delete_stream(dst_to_src_stream)"""
