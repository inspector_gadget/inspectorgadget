from scapy.layers.inet import *

from constants.State import State
from constants.TcpFlags import TcpFlags


class BadPacket(Exception):
    pass


class FirstPacketIsNotSynError(BadPacket):
    pass


class PacketsService(object):
    def get_sent_packet_state(self, pac, our_state, their_state):
        return self.get_next_state(pac, our_state, their_state, True)[0]

    def get_received_packet_state(self, pac, our_state, their_state):
        return self.get_next_state(pac, our_state, their_state, False)[1]

    def get_next_state(self, pac, our_state, their_state, are_we_sender):
        try:
            return self.get_next_state_of_connection(pac, our_state, their_state, are_we_sender)
        except BadPacket:
            pass

        try:
            return self.get_next_state_of_conversation(pac, our_state, their_state, are_we_sender)
        except BadPacket:
            pass

        try:
            return self.get_next_state_of_termination(pac, our_state, their_state, are_we_sender)
        except BadPacket:
            pass

        if (our_state, their_state) == (None, None):
            raise FirstPacketIsNotSynError()
        else:
            raise BadPacket()

    def get_next_state_of_connection(self, pac, our_state, their_state, are_we_sender):
        if self.is_fin(pac):
            raise BadPacket()

        # SYN
        if (our_state, their_state) == (None, None):
            if self.is_syn(pac) and not self.is_ack(pac):
                if are_we_sender:
                    return State.SYN, None
                else:
                    return None, State.SYN

        # SYN-ACK
        if (our_state, their_state) == (State.SYN, None):
            if self.is_syn(pac) and self.is_ack(pac):
                if not are_we_sender:
                    return State.SYN, State.SYN_ACK

        if (our_state, their_state) == (None, State.SYN):
            if self.is_syn(pac) and self.is_ack(pac):
                if are_we_sender:
                    return State.SYN_ACK, State.SYN

        # ACK
        if (our_state, their_state) == (State.SYN, State.SYN_ACK):
            if not self.is_syn(pac) and self.is_ack(pac):
                if are_we_sender:
                    return State.ACK, State.SYN_ACK

        if (our_state, their_state) == (State.SYN_ACK, State.SYN):
            if not self.is_syn(pac) and self.is_ack(pac):
                if not are_we_sender:
                    return State.SYN_ACK, State.ACK

        raise BadPacket()

    def get_next_state_of_conversation(self, pac, our_state, their_state, are_we_sender):
        if self.is_syn(pac) or self.is_fin(pac):
            raise BadPacket()

        if (our_state, their_state) in [(State.ACK, State.ACK), (State.SYN_ACK, State.ACK), (State.ACK, State.SYN_ACK)]:
            if self.is_ack(pac):
                if are_we_sender:
                    return State.ACK, their_state
                else:
                    return our_state, State.ACK

        raise BadPacket()

    def get_next_state_of_termination(self, pac, our_state, their_state, are_we_sender):

        if self.is_syn(pac):
            raise BadPacket()

        # FIN
        if (our_state, their_state) in [(State.ACK, State.ACK), (State.ACK, State.SYN_ACK), (State.SYN_ACK, State.ACK)]:
            if self.is_fin(pac):
                if are_we_sender:
                    return State.FIN, their_state

        if (our_state, their_state) in [(State.ACK, State.ACK), (State.SYN_ACK, State.ACK),
                                        (State.ACK, State.SYN_ACK)]:
            if self.is_fin(pac):
                if not are_we_sender:
                    return our_state, State.FIN

        # ACK
        if (our_state, their_state) in [(State.FIN, State.ACK), (State.FIN, State.SYN_ACK)]:
            if self.is_ack(pac) and not self.is_fin(pac):
                if not are_we_sender:
                    return State.FIN, State.ACK

        if (our_state, their_state) in [(State.ACK, State.FIN), (State.SYN_ACK, State.FIN)]:
            if self.is_ack(pac) and not self.is_fin(pac):
                if are_we_sender:
                    return State.ACK, State.FIN

        # FIN
        if (our_state, their_state) == (State.FIN, State.ACK):
            if self.is_fin(pac):
                if not are_we_sender:
                    return State.FIN, State.FIN

        if (our_state, their_state) == (State.ACK, State.FIN):
            if self.is_fin(pac):
                if are_we_sender:
                    return State.FIN, State.FIN

        # ACK
        if (our_state, their_state) == (State.FIN, State.FIN):
            if self.is_ack(pac) and not self.is_fin(pac):
                if are_we_sender:
                    return State.ACK, State.FIN
                else:
                    return State.FIN, State.ACK

        raise BadPacket()

    def get_packet_state(self, pac):
        if not self.is_fin(pac):
            if self.is_syn(pac) and not self.is_ack(pac):
                return State.SYN
            elif self.is_syn(pac) and self.is_ack(pac):
                return State.SYN_ACK
            elif not self.is_syn(pac) and self.is_ack(pac):
                return State.ACK
        elif not self.is_syn(pac):
            return State.FIN

        return None

    def is_syn(self, pac):
        return pac[TCP].flags & TcpFlags.SYN

    def is_ack(self, pac):
        return pac[TCP].flags & TcpFlags.ACK

    def is_push(self, pac):
        return pac[TCP].flags & TcpFlags.PSH

    def is_fin(self, pac):
        return pac[TCP].flags & TcpFlags.FIN

    def get_seq_number(self, pac):
        return pac[TCP].ack

    def get_ack_number(self, pac):
        return pac[TCP].seq

    def get_window_number(self, pac):
        return pac[TCP].window
