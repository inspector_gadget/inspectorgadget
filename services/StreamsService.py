from constants.State import State
from models.TcpStream import TcpStream


class StreamsService(object):
    def __init__(self):
        self.streams = []

    def ensure_exists(self, our_ip, our_port, their_ip, their_port):
        stream = self.find(our_ip, our_port, their_ip, their_port)

        if stream is None:
            stream = self.create(TcpStream(our_ip, our_port, their_ip, their_port))

        return stream

    def find(self, our_ip, our_port, their_ip, their_port):
        for stream in self.streams:
            if stream.our_ip == our_ip and stream.our_port == our_port and stream.their_ip == their_ip and stream.their_port == their_port:
                return stream

    def create(self, tcp_stream):
        self.streams.append(tcp_stream)
        return tcp_stream

    def clear(self):
        self.streams = []

    def remove(self, stream):
        self.streams.remove(stream)
