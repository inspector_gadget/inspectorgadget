import socket
from scapy.layers.inet import IP


class IpNotValidError(Exception):
    pass


class IpsService(object):
    def __init__(self):
        self.ips = set()

    def add_ip(self, ip):
        if self.validate_ip(ip):
            self.ips.add(ip)
        else:
            raise IpNotValidError()

    def remove_ip(self, ip):
        self.ips.remove(ip)

    def get_ips(self):
        return self.ips  # TODO we can have multiple ip's

    @staticmethod
    def validate_ip(ip):
        lst = ip.split(".")
        try:
            for i in ip.split("."):
                if not int(i) in range(256):
                    return False
            if len(lst) == 4:
                if not ((lst[0] == '192' and lst[1] == '0' and lst[2] == '2') or (
                                    lst[0] == '198' and lst[1] == '51' and lst[2] == '100') or (
                                    lst[0] == '203' and lst[1] == '0' and lst[2] == '113')):
                    return True
            return False
        except ValueError:
            return False

    def clear_ips(self):
        self.ips.clear()

    def are_we_packet_source(self, pac):
        return pac[IP].src in self.ips

    def are_we_packet_destination(self, pac):
        return pac[IP].dst in self.ips
