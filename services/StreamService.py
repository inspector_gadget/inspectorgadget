from scapy.layers.inet import IP

from constants.State import State


class SeqAckException(Exception):
    pass


class StreamEnded(Exception):
    pass


class StreamService(object):
    def __init__(self, packets_service):
        self.packets_service = packets_service

    def are_we_packet_source(self, stream, pac):
        return pac[IP].src == stream.our_ip and pac[IP].sport == stream.our_port

    def are_we_packet_destination(self, stream, pac):
        return pac[IP].dst == stream.our_ip and pac[IP].dport == stream.our_port

    def analyze_packet(self, stream, pac):

        are_we_packet_source = self.are_we_packet_source(stream, pac)

        if stream.sent_state == State.FIN and stream.received_state == State.FIN:
            stream_ending = True
        else:
            stream_ending = False

        prev_sent_state, prev_received_state = stream.sent_state, stream.received_state

        stream.sent_state, stream.received_state = self.packets_service.get_next_state(pac,
                                                                                       stream.sent_state,
                                                                                       stream.received_state,
                                                                                       are_we_packet_source)

        stream.prev_sent_state, stream.prev_received_state = prev_sent_state, prev_received_state

        if not self.is_seq_ack_valid(stream, pac):
            raise SeqAckException()

        return stream_ending and (stream.sent_state == State.ACK or stream.received_state == State.ACK)

    """def get_stream_states_history(self, stream):
        return self.packets_service.get_packets_states_history(stream.packets)

    def get_state(self, stream):
        return list(self.get_stream_states_history(stream))[-1]

    def get_current_and_prev_states(self, stream):
        states_history = list(self.get_stream_states_history(stream))

        state = states_history[-1]

        try:
            prev_state = states_history[-2]
        except IndexError:
            prev_state = None

        return prev_state, state"""

    def is_seq_ack_valid(self, stream, pac):
        """if len(stream.packets) > 1:
            seq_num = self.packets_service.get_seq_number(stream.packets[-1])
            ack_num = self.packets_service.get_ack_number(stream.packets[-1])
            window = self.packets_service.get_window_number(stream.packets[-1])

            prev_seq_num = self.packets_service.get_seq_number(stream.packets[-2])
            prev_ack_num = self.packets_service.get_ack_number(stream.packets[-2])

            return seq_num == prev_ack_num and window + prev_seq_num >= ack_num >= prev_seq_num

        else:
            return True"""

        return True
