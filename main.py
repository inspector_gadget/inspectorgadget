import socket

from services.IpsService import IpsService
from services.PacketsService import PacketsService
from services.SniffingService import SniffingService
from services.StreamService import StreamService
from services.StreamsService import StreamsService


def main():
    ips_service = IpsService()

    hostname = socket.gethostname()
    ips_service.add_ip(socket.gethostbyname(hostname))

    packets_service = PacketsService()
    streams_service = StreamsService()
    stream_service = StreamService(packets_service)

    sniffing_service = SniffingService(
        ips_service,
        packets_service,
        streams_service,
        stream_service
    )

    sniffing_service.sniff()

if __name__ == '__main__':
    main()
