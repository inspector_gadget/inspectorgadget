# www.ietf.org/rfc/rfc793.txt

from enum import IntEnum


# Last sent message type
class State(IntEnum):
    SYN = 0
    SYN_ACK = 1
    ACK = 2
    FIN = 3


state_names = {
    State.SYN: "SYN",
    State.SYN_ACK: "SYN-ACK",
    State.ACK: "ACK",
    State.FIN: "FIN",
    None: "None"
}

states = [None, State.SYN, State.SYN_ACK, State.ACK, State.FIN]

sent_and_received_states_combinations = []

for _sent_state in states:
    for _received_state in states:
        sent_and_received_states_combinations.append((_sent_state, _received_state))
