import pytest

from services.IpsService import IpsService


class TestIpsService(object):
    @pytest.fixture
    def ips_service(self):
        return IpsService()

    def test_validate(self):
        assert IpsService.validate_ip("123.23.23.23")
        assert not IpsService.validate_ip("192.0.2.5")
        assert not IpsService.validate_ip("123.267.23.32")
        assert not IpsService.validate_ip("123.23.23")
        assert not IpsService.validate_ip("123.abc.23.23")
