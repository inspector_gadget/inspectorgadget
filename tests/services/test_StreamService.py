import pytest

from scapy.layers.inet import *

from constants.State import State
from models.TcpStream import TcpStream
from services.PacketsService import PacketsService
from services.StreamService import StreamService


class TestStreamService(object):
    @pytest.fixture
    def packets_service(self):
        return PacketsService()

    @pytest.fixture
    def stream_service(self, packets_service):
        return StreamService(packets_service)

    @pytest.fixture
    def get_packet_to_send(self, stream):
        return IP(src=stream.our_ip, dst=stream.their_ip) / TCP(sport=stream.our_port, dport=stream.their_port,
                                                                flags="S")

    @pytest.fixture
    def get_packet_flags(self):
        def get(state, is_push=False):
            assert state is not None

            if state == State.SYN:
                flags = "S"
            elif state == State.SYN_ACK:
                flags = "SA"
            elif state == State.ACK:
                flags = "A"
            elif state == State.FIN:
                flags = "F"
            else:
                flags = ""

            if is_push:
                flags += "P"

            return flags

        return get

    @pytest.fixture
    def get_packet_to_send(self, stream, get_packet_flags):
        def get(state, is_push=False):
            return IP(src=stream.our_ip, dst=stream.their_ip) / TCP(sport=stream.our_port,
                                                                    dport=stream.their_port,
                                                                    flags=get_packet_flags(state, is_push))

        return get

    @pytest.fixture
    def get_packet_to_receive(self, stream, get_packet_flags):
        def get(state, is_push=False):
            return IP(src=stream.their_ip, dst=stream.our_ip) / TCP(sport=stream.their_port,
                                                                    dport=stream.our_port,
                                                                    flags=get_packet_flags(state, is_push))

        return get

    @pytest.fixture
    def stream(self):
        our_ip, our_port = "157.4.23.1", 2333
        server_ip, server_port = "134.56.7.8", 3043

        return TcpStream(our_ip, our_port, server_ip, server_port)

    def test_stream_states_are_empty_on_create(self, stream):
        assert stream.sent_state is None
        assert stream.received_state is None

    def test_three_way_handshake_client_side(self, stream_service, stream, get_packet_to_send, get_packet_to_receive):
        # SYN
        stream_service.analyze_packet(stream, get_packet_to_send(State.SYN))

        assert stream.sent_state == State.SYN
        assert stream.received_state is None

        # SYN-ACK
        stream_service.analyze_packet(stream, get_packet_to_receive(State.SYN_ACK))

        assert stream.sent_state == State.SYN
        assert stream.received_state == State.SYN_ACK

        # ACK
        stream_service.analyze_packet(stream, get_packet_to_send(State.ACK))

        assert stream.sent_state == State.ACK
        assert stream.received_state == State.SYN_ACK

    def test_three_way_handshake_server_side(self, stream_service, stream, get_packet_to_send, get_packet_to_receive):

        # SYN
        stream_service.analyze_packet(stream, get_packet_to_receive(State.SYN))

        assert stream.sent_state is None
        assert stream.received_state == State.SYN

        # SYN-ACK
        stream_service.analyze_packet(stream, get_packet_to_send(State.SYN_ACK))

        assert stream.sent_state == State.SYN_ACK
        assert stream.received_state == State.SYN

        # ACK
        stream_service.analyze_packet(stream, get_packet_to_receive(State.ACK))

        assert stream.sent_state == State.SYN_ACK
        assert stream.received_state == State.ACK

    def test_conversation_send(self, stream_service, stream, get_packet_to_send):
        stream.sent_state = State.SYN_ACK
        stream.received_state = State.ACK

        # ACK

        stream_service.analyze_packet(stream, get_packet_to_send(State.ACK))

        assert stream.sent_state == State.ACK
        assert stream.received_state == State.ACK

    def test_conversation_receive(self, stream_service, stream, get_packet_to_receive):
        stream.sent_state = State.ACK
        stream.received_state = State.SYN_ACK

        # ACK

        stream_service.analyze_packet(stream, get_packet_to_receive(State.ACK))

        assert stream.sent_state == State.ACK
        assert stream.received_state == State.ACK

    def test_disconnect_willing_side(self, stream_service, stream, get_packet_to_send, get_packet_to_receive):
        stream.sent_state = State.ACK
        stream.received_state = State.SYN_ACK

        # Send FIN

        stream_service.analyze_packet(stream, get_packet_to_send(State.FIN))

        assert stream.sent_state == State.FIN
        assert stream.received_state == State.SYN_ACK

        # Receive ACK

        stream_service.analyze_packet(stream, get_packet_to_receive(State.ACK))

        assert stream.sent_state == State.FIN
        assert stream.received_state == State.ACK

        # Receive FIN

        stream_service.analyze_packet(stream, get_packet_to_receive(State.FIN))

        assert stream.sent_state == State.FIN
        assert stream.received_state == State.FIN

        # Send ACK

        stream_service.analyze_packet(stream, get_packet_to_send(State.ACK))

        assert stream.sent_state == State.ACK
        assert stream.received_state == State.FIN

    def test_disconnect_accepting_side(self, stream_service, stream, get_packet_to_send, get_packet_to_receive):
        stream.sent_state = State.SYN_ACK
        stream.received_state = State.ACK

        # Receive FIN

        stream_service.analyze_packet(stream, get_packet_to_receive(State.FIN))

        assert stream.sent_state == State.SYN_ACK
        assert stream.received_state == State.FIN

        # Send ACK

        stream_service.analyze_packet(stream, get_packet_to_send(State.ACK))

        assert stream.sent_state == State.ACK
        assert stream.received_state == State.FIN

        # Send FIN

        stream_service.analyze_packet(stream, get_packet_to_send(State.FIN))

        assert stream.sent_state == State.FIN
        assert stream.received_state == State.FIN

        # Receive ACK

        stream_service.analyze_packet(stream, get_packet_to_receive(State.ACK))

        assert stream.sent_state == State.FIN
        assert stream.received_state == State.ACK
