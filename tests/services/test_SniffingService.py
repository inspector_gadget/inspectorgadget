from scapy.layers.inet import *

import pytest

from constants.State import State
from services.IpsService import IpsService
from services.PacketsService import PacketsService
from services.SniffingService import SniffingService
from services.StreamService import StreamService
from services.StreamsService import StreamsService


class TestSniffingService(object):
    @pytest.fixture
    def our_ip(self):
        return "10.0.0.3"

    @pytest.fixture
    def our_port(self):
        return 3003

    @pytest.fixture
    def their_ip(self):
        return "125.23.4.1"

    @pytest.fixture
    def their_port(self):
        return 4444

    @pytest.fixture
    def ips_service(self, our_ip):
        ips_service = IpsService()
        ips_service.add_ip(our_ip)
        return ips_service

    @pytest.fixture
    def packets_service(self):
        return PacketsService()

    @pytest.fixture
    def stream_service(self, packets_service):
        return StreamService(packets_service)

    @pytest.fixture
    def streams_service(self):
        return StreamsService()

    @pytest.fixture
    def sniffing_service(self, ips_service, packets_service, streams_service, stream_service):
        return SniffingService(ips_service, packets_service, streams_service, stream_service)

    @pytest.fixture
    def _fake_sniff(self, sniffing_service):
        def sniff(packets):
            for pac in packets:
                sniffing_service(pac)

        return sniff

    def test_analyze_packet(self, our_ip, our_port, their_ip, their_port, sniffing_service, streams_service):
        syn_packet = IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="S")
        syn_ack_packet = IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="SA")
        ack_packet = IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="A")

        is_stream_ended = sniffing_service.analyze_packet(syn_packet)
        assert is_stream_ended is None
        stream = streams_service.find(our_ip, our_port, their_ip, their_port)
        assert stream is not None

        is_stream_ended = sniffing_service.analyze_packet(syn_ack_packet)
        assert is_stream_ended is None
        assert stream is streams_service.find(our_ip, our_port, their_ip, their_port)

        # Test disconnect

        stream.sent_state = State.FIN
        stream.received_state = State.FIN

        sniffing_service.analyze_packet(ack_packet)
        assert streams_service.find(our_ip, our_port, their_ip, their_port) is None



