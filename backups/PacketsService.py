def get_received_packet_state(self, pac, sent_state, received_state):
    is_syn = pac[TCP].flags & TcpFlags.SYN
    is_ack = pac[TCP].flags & TcpFlags.ACK
    # is_push = pac[TCP].flags & TcpFlags.PSH
    is_fin = pac[TCP].flags & TcpFlags.FIN

    if is_fin and not is_syn:
        if (sent_state, received_state) in [(State.ACK, State.FIN), (State.SYN_ACK, State.FIN),
                                            (State.ACK, State.ACK), (State.SYN_ACK, State.ACK),
                                            (State.ACK, State.SYN_ACK)]:
            return State.FIN

    if is_syn and is_ack and not is_fin:
        return State.SYN_ACK

    if is_syn and not is_ack and not is_fin:
        return State.SYN

    elif sent_state is None and received_state is None:
        if is_syn and not is_ack and not is_fin:
            return State.SYN
        else:
            raise FirstPacketIsNotSynError()

    elif sent_state == State.SYN and received_state is None:
        if is_syn and is_ack and not is_fin:
            return State.SYN_ACK
        else:
            raise BadPacket()

    else:
        if is_ack and not is_syn and not is_fin:
            return State.ACK
        else:
            raise BadPacket()


def get_sent_packet_state(self, pac, sent_state, received_state):
    return self.get_received_packet_state(pac, received_state, sent_state)
    is_syn = pac[TCP].flags & TcpFlags.SYN
    is_ack = pac[TCP].flags & TcpFlags.ACK
    # is_push = pac[TCP].flags & TcpFlags.PSH
    is_fin = pac[TCP].flags & TcpFlags.FIN

    if received_state in []

    if is_syn and not is_ack and prev_state is None:
        return State.SENT_SYN
    elif is_syn and is_ack and prev_state == State.GOT_SYN:
        return State.SENT_SYN_ACK
    elif not is_syn and is_ack and prev_state == State.GOT_SYN_ACK:
        return State.SENT_ACK

    if is_ack and is_push and not is_syn and not is_fin:
        return State.SENT_PUSH
    elif is_ack and not is_push and not is_syn and prev_state == State.GOT_PUSH:
        return State.SENT_POST_PUSH_ACK

    if is_fin and not is_push and not is_syn and not is_ack:
        return State.SENT_FIN
    elif is_fin and not is_push and not is_syn and is_ack and prev_state == State.GOT_FIN:
        return State.SENT_FIN_ACK
    elif not is_fin and not is_push and not is_syn and is_ack and prev_state == State.GOT_FIN_ACK:
        return State.SENT_POST_FIN_ACK

    if is_syn and prev_state is None:
        raise FirstPacketIsNotSynError()