import pytest

from scapy.layers.inet import *

from constants.State import State
from models.TcpStream import TcpStream
from services.PacketsService import PacketsService
from services.StreamService import StreamService


class TestStreamService(object):
    @pytest.fixture
    def packets_service(self):
        return PacketsService()

    @pytest.fixture
    def stream_service(self, packets_service):
        return StreamService(packets_service)

    def test_three_way_handshake_client_side(self, stream_service):
        our_ip, our_port = "157.4.23.1", 2333
        server_ip, server_port = "134.56.7.8", 3043

        we_to_server_stream = TcpStream(our_ip, our_port, server_ip, server_port)
        stream_service.analyze_packet(we_to_server_stream,
                                      IP(src=our_ip, dst=server_ip) / TCP(sport=our_port, dport=server_port, flags="S"))

        assert we_to_server_stream.our_state == State.SENT_SYN

        stream_service.analyze_packet(we_to_server_stream,
                                      IP(src=server_ip, dst=our_ip) / TCP(sport=server_port, dport=our_port,
                                                                          flags="SA"))
        assert we_to_server_stream.our_state == State.GOT_SYN_ACK

        stream_service.analyze_packet(we_to_server_stream,
                                      IP(src=our_ip, dst=server_ip) / TCP(sport=our_port, dport=server_port, flags="A"))
        assert we_to_server_stream.our_state == State.SENT_ACK

    def test_three_way_handshake_server_side(self, streams_service, sniffing_service):
        our_ip, our_port = "132.54.33.4", 2333
        client_ip, client_port = "164.76.74.83", 3043

        sniffing_service.fake_sniff(IP(src=client_ip, dst=our_ip) / TCP(sport=client_port, dport=our_port, flags="S"))
        client_to_us_stream = streams_service.find(our_ip, our_port, client_ip, client_port)
        assert client_to_us_stream is not None
        assert client_to_us_stream.our_state == State.GOT_SYN

        sniffing_service.fake_sniff(IP(src=our_ip, dst=client_ip) / TCP(sport=our_port, dport=client_port, flags="SA"))
        assert client_to_us_stream.our_state == State.SENT_SYN_ACK

        sniffing_service.fake_sniff(IP(src=client_ip, dst=our_ip) / TCP(sport=client_port, dport=our_port, flags="A"))
        assert client_to_us_stream.our_state == State.GOT_ACK

    def test_conversation_sender_side(self, streams_service, sniffing_service):
        our_ip, our_port = "132.54.33.4", 2333
        their_ip, their_port = "164.76.74.83", 3043

        us_to_them_stream = streams_service.create(TcpStream(our_ip, our_port, their_ip, their_port))
        us_to_them_stream.our_state = State.GOT_SYN

        sniffing_service.fake_sniff(IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="PA"))
        assert us_to_them_stream.our_state == State.SENT_PUSH

        sniffing_service.fake_sniff(IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="A"))
        assert us_to_them_stream.our_state == State.GOT_POST_PUSH_ACK

    def test_conversation_receiver_side(self, streams_service):
        our_ip, our_port = "142.55.33.5", 2333
        their_ip, their_port = "114.77.84.82", 3043

        them_to_us_stream = streams_service.create(TcpStream(our_ip, our_port, their_ip, their_port))
        them_to_us_stream.our_state = State.GOT_SYN

        sniffing_service.fake_sniff(IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="PA"))
        assert them_to_us_stream.our_state == State.GOT_PUSH

        sniffing_service.fake_sniff(IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="A"))
        assert them_to_us_stream.our_state == State.SENT_POST_PUSH_ACK

    def test_disconnect_willing_side(self, streams_service):
        our_ip, our_port = "122.55.43.8", 2253
        their_ip, their_port = "124.75.64.42", 3044

        us_to_them_stream = streams_service.create(TcpStream(our_ip, our_port, their_ip, their_port))
        us_to_them_stream.our_state = State.GOT_SYN

        sniffing_service.fake_sniff(IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="F"))
        assert us_to_them_stream.our_state == State.SENT_FIN

        sniffing_service.fake_sniff(IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="FA"))
        assert us_to_them_stream.our_state == State.GOT_FIN_ACK

        sniffing_service.fake_sniff(IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="A"))
        assert us_to_them_stream.our_state == State.SENT_POST_FIN_ACK
        assert streams_service.find(our_ip, our_port, their_ip, their_port) is None

    def test_disconnect_acceptor_side(self, streams_service):
        our_ip, our_port = "122.55.43.8", 2253
        their_ip, their_port = "124.75.64.42", 3044

        them_to_us_stream = streams_service.create(TcpStream(our_ip, our_port, their_ip, their_port))
        them_to_us_stream.our_state = State.GOT_SYN

        sniffing_service.fake_sniff(IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="F"))
        assert them_to_us_stream.our_state == State.GOT_FIN

        sniffing_service.fake_sniff(IP(src=our_ip, dst=their_ip) / TCP(sport=our_port, dport=their_port, flags="FA"))
        assert them_to_us_stream.our_state == State.SENT_FIN_ACK

        sniffing_service.fake_sniff(IP(src=their_ip, dst=our_ip) / TCP(sport=their_port, dport=our_port, flags="A"))
        assert them_to_us_stream.our_state == State.GOT_POST_FIN_ACK
        assert streams_service.find(our_ip, our_port, their_ip, their_port) is None
