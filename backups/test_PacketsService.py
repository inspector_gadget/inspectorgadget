from scapy.layers.inet import *

import pytest

from constants.State import State, sent_and_received_states_combinations
from services.PacketsService import PacketsService, BadPacket

_SYN_PACKET = TCP(flags="S")
_SYN_ACK_PACKET = TCP(flags="SA")
_ACK_PACKET = TCP(flags="A")
_PUSH_ACK_PACKET = TCP(flags="PA")
_FIN_PACKET = TCP(flags="F")


class _PacketsServiceTest(object):
    @pytest.fixture
    def packets_service(self):
        return PacketsService()


class TestClientTwoWayHandshake(_PacketsServiceTest):
    # SYN
    states_allowing_sending_syn = [(None, None)]

    def test_sending_syn(self, packets_service):
        for sent_state, received_state in self.states_allowing_sending_syn:
            assert packets_service.get_sent_packet_state(_SYN_PACKET, sent_state, received_state) == State.SYN

    # SYN ACK
    states_allowing_receiving_syn_ack = [(State.SYN, None)]

    def test_receiving_syn_ack(self, packets_service):
        for sent_state, received_state in self.states_allowing_receiving_syn_ack:
            assert packets_service.get_received_packet_state(_SYN_ACK_PACKET, sent_state,
                                                             received_state) == State.SYN_ACK

    # ACK
    states_sending_ack = [(State.SYN, State.SYN_ACK)]

    def test_allowing_sending_ack(self, packets_service):
        for sent_state, received_state in self.states_sending_ack:
            assert packets_service.get_sent_packet_state(_ACK_PACKET, sent_state, received_state) == State.ACK


class TestServerTwoWayHandshake(_PacketsServiceTest):
    # SYN
    states_allowing_receiving_syn = [(x, y) for (y, x) in TestClientTwoWayHandshake.states_allowing_sending_syn]

    def test_receiving_syn(self, packets_service):
        assert packets_service.get_received_packet_state(_SYN_PACKET, None, None) == State.SYN

    # SYN ACK
    states_allowing_sending_syn_ack = [(x, y) for (y, x) in TestClientTwoWayHandshake.states_allowing_receiving_syn_ack]

    def test_sending_syn_ack(self, packets_service):
        assert packets_service.get_sent_packet_state(_SYN_ACK_PACKET, None, State.SYN) == State.SYN_ACK

    # ACk
    states_allowing_receiving_ack = [(x, y) for (y, x) in TestClientTwoWayHandshake.states_sending_ack]

    def test_receiving_post_syn_ack_ack(self, packets_service):
        assert packets_service.get_received_packet_state(_ACK_PACKET, State.SYN_ACK, State.SYN) == State.ACK


class TestConversationSenderSide(_PacketsServiceTest):
    # ACk
    states_allowing_sending_ack = [(State.SYN_ACK, State.ACK), (State.ACK, State.SYN_ACK), (State.ACK, State.ACK)]

    def test_sending_ack(self, packets_service):
        for sent_side, received_state in self.states_allowing_sending_ack:
            assert packets_service.get_sent_packet_state(_ACK_PACKET, sent_side, received_state) == State.ACK

        """for sent_state, received_state in [
            (State.SYN, State.SYN), (State.SYN, State.ACK), (State.ACK, State.SYN),
            (State.SYN_ACK, State.SYN_ACK), (State.FIN, State.ACK), (State.ACK, State.FIN), (State.FIN, State.FIN)
        ]:
            with pytest.raises(BadPacket):
                packets_service.get_sent_packet_state(_ACK_PACKET, sent_state, received_state)"""


class TestConversationReceivingSide(_PacketsServiceTest):
    # ACk
    states_allowing_receiving_ack = [(x, y) for (y, x) in TestConversationSenderSide.states_allowing_sending_ack]

    def test_receiving_ack(self, packets_service):
        for sent_side, received_state in self.states_allowing_receiving_ack:
            assert packets_service.get_received_packet_state(_ACK_PACKET, sent_side, received_state) == State.ACK

        """for sent_state, received_state in [
            (State.SYN, State.SYN), (State.SYN, State.ACK), (State.ACK, State.SYN),
            (State.SYN_ACK, State.SYN_ACK), (State.FIN, State.ACK), (State.ACK, State.FIN), (State.FIN, State.FIN)
        ]:
            with pytest.raises(BadPacket):
                packets_service.get_received_packet_state(_ACK_PACKET, sent_state, received_state)"""


# See https://jindongpu.files.wordpress.com/2012/04/tcp.png
class TestDisconnectAcceptingSide(_PacketsServiceTest):
    # FIN
    states_allowing_receiving_fin = [(State.ACK, State.ACK), (State.SYN_ACK, State.ACK), (State.ACK, State.SYN_ACK)]

    def test_receiving_fin(self, packets_service):
        for sent_state, received_state in self.states_allowing_receiving_fin:
            assert packets_service.get_received_packet_state(_FIN_PACKET, sent_state, received_state) == State.FIN

    # ACK
    states_allowing_sending_ack = [(sent_state, State.FIN) for sent_state in [State.ACK, State.SYN_ACK]]

    def test_sending_ack(self, packets_service):
        for sent_state, received_state in self.states_allowing_sending_ack:
            assert packets_service.get_sent_packet_state(_ACK_PACKET, sent_state, State.FIN) == State.ACK

    # FIN
    states_allowing_sending_fin = [(State.ACK, State.FIN)]

    def test_sending_fin(self, packets_service):
        for sent_state, received_state in self.states_allowing_sending_fin:
            assert packets_service.get_sent_packet_state(_FIN_PACKET, sent_state, received_state) == State.FIN

    # ACK
    states_allowing_receiving_ack = [(State.FIN, State.FIN)]

    def test_receiving_ack(self, packets_service):
        for sent_state, received_state in self.states_allowing_receiving_ack:
            assert packets_service.get_received_packet_state(_ACK_PACKET, sent_state, received_state) == State.ACK


# See https://jindongpu.files.wordpress.com/2012/04/tcp.png
class TestDisconnectRequestingSide(_PacketsServiceTest):
    states_allowing_sending_fin = [(x, y) for (y, x) in TestDisconnectAcceptingSide.states_allowing_receiving_fin]

    def test_sending_fin(self, packets_service):
        for sent_state, received_state in self.states_allowing_sending_fin:
            assert packets_service.get_sent_packet_state(_FIN_PACKET, sent_state,
                                                         received_state) == State.FIN

    states_allowing_receiving_ack = [(x, y) for (y, x) in TestDisconnectAcceptingSide.states_allowing_sending_ack]

    def test_receiving_ack(self, packets_service):
        for received_state in [State.ACK, State.ACK, State.SYN_ACK]:
            assert packets_service.get_received_packet_state(_ACK_PACKET, State.FIN,
                                                             received_state) == State.ACK

    states_allowing_receiving_fin = [(x, y) for (y, x) in TestDisconnectAcceptingSide.states_allowing_sending_fin]

    def test_receiving_fin(self, packets_service):
        for sent_state, received_state in self.states_allowing_sending_fin:
            assert packets_service.get_received_packet_state(_FIN_PACKET, sent_state,
                                                             received_state) == State.FIN

    states_allowing_sending_ack = [(x, y) for (y, x) in TestDisconnectAcceptingSide.states_allowing_receiving_ack]

    def test_sending_ack(self, packets_service):
        assert packets_service.get_sent_packet_state(_ACK_PACKET, State.FIN, State.FIN) == State.ACK


class TestSynBadPackets(_PacketsServiceTest):
    def test_sending_syn(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestClientTwoWayHandshake.states_allowing_sending_syn:
                with pytest.raises(BadPacket):
                    packets_service.get_sent_packet_state(_SYN_PACKET, sent_state, received_state)

    def test_receiving_syn(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestServerTwoWayHandshake.states_allowing_receiving_syn:
                with pytest.raises(BadPacket):
                    packets_service.get_received_packet_state(_SYN_PACKET, sent_state, received_state)


class TestSynAckBadPackets(_PacketsServiceTest):
    def test_sending_syn_ack(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestServerTwoWayHandshake.states_allowing_sending_syn_ack:
                with pytest.raises(BadPacket):
                    packets_service.get_sent_packet_state(_SYN_ACK_PACKET, sent_state, received_state)

    def test_receiving_syn_ack(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestClientTwoWayHandshake.states_allowing_receiving_syn_ack:
                with pytest.raises(BadPacket):
                    packets_service.get_received_packet_state(_SYN_ACK_PACKET, sent_state, received_state)


class TestAckBadPackets(_PacketsServiceTest):
    def test_sending_ack(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestClientTwoWayHandshake.states_sending_ack \
                    + TestConversationSenderSide.states_allowing_sending_ack \
                    + TestDisconnectAcceptingSide.states_allowing_sending_ack \
                    + TestDisconnectRequestingSide.states_allowing_sending_ack:
                with pytest.raises(BadPacket):
                    packets_service.get_sent_packet_state(_ACK_PACKET, sent_state, received_state)

    def test_receiving_ack(self, packets_service):
        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestServerTwoWayHandshake.states_allowing_receiving_ack \
                    + TestConversationReceivingSide.states_allowing_receiving_ack \
                    + TestDisconnectAcceptingSide.states_allowing_receiving_ack \
                    + TestDisconnectRequestingSide.states_allowing_receiving_ack:
                with pytest.raises(BadPacket):
                    packets_service.get_received_packet_state(_ACK_PACKET, sent_state, received_state)


class TestFinBadPackets(_PacketsServiceTest):
    def test_sending_fin(self, packets_service):

        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestDisconnectAcceptingSide.states_allowing_sending_fin \
                    + TestDisconnectRequestingSide.states_allowing_sending_fin:
                with pytest.raises(BadPacket):
                    packets_service.get_sent_packet_state(_FIN_PACKET, sent_state, received_state)

    def test_receiving_fin(self, packets_service):

        for sent_state, received_state in sent_and_received_states_combinations:
            if (sent_state, received_state) not in TestDisconnectAcceptingSide.states_allowing_receiving_fin \
                    + TestDisconnectRequestingSide.states_allowing_receiving_fin:
                with pytest.raises(BadPacket):
                    packets_service.get_received_packet_state(_FIN_PACKET, sent_state, received_state)
